///<reference path="component-manager.ts"/>
///<reference path="component-services/attributes.ts"/>
///<reference path="component-services/model-properties.ts"/>
///<reference path="component-services/style-properties.ts"/>
///<reference path="../components/components.ts"/>
///<reference path="../component-factory.ts"/>
///<reference path="../../Mapper.ts"/>

/// <reference path="../../../../typings/jquery/jquery.d.ts" />

class Layout
{
	componentManager:ComponentManager;
	
	dragTarget:Component;
	currentComponent:Component;
	workbench:Element;
	mapper:Mapper;

	constructor(workbenchId:string, mapper:Mapper)
	{
		this.workbench = document.getElementById(workbenchId);
		this.mapper = mapper;

		this.setUpComponentManager();
	}

	private setUpComponentManager()
	{
		this.componentManager = new ComponentManager();
		this.componentManager.setAttributes(new Attributes(document.querySelector('#attribute-editor')));
		this.componentManager.setModelProperties(new ModelProperties(document.querySelector('#models-editor')));
		this.componentManager.setStyles(new StyleProperties(document.querySelector('#style-editor')));
	}

	registerDragTarget = (draggedComponent:string) =>
	{
		this.dragTarget = this.mapper.componentFactory.getComponent(draggedComponent);
	};

	onDrag = (ev:Event) =>
	{
		let dragTarget = (<any>ev.target);
		let draggedComponent = dragTarget.dataset.component;
		this.registerDragTarget(draggedComponent);
	};

	onDrop = (ev:Event, dropTarget:Element) =>
	{
		ev.preventDefault();
		this.createElement(this.dragTarget, dropTarget);
	};

	onDragOver = (ev) =>
	{
		ev.preventDefault();
		ev.dataTransfer.dropEffect = "move";
	};

	listProperties = (component:Component) =>
	{
		this.componentManager.displayAttributes(component);
		this.componentManager.displayModels(component);
		this.componentManager.displayStyles(component);
	};

	onElementCreated = (component:Component) =>
	{
		this.mapper.addComponent(component);
		let element = component.element;
		this.onSelectObject(component);
		element.setAttribute('draggable', 'true');
		element.addEventListener('dragstart', this.onDrag);
		element.addEventListener('click', function ()
		{
			this.onSelectObject(component);
		}.bind(this));
	};

	onSelectObject = (component:Component) =>
	{
		this.currentComponent = component;
		this.mapper.addComponent(component);

		let element = component.element;
		(<any>element).dataset.id = component.getObjectId();
		this.listProperties(component);
	};

	updateProperties = (component:Component) =>
	{
		this.componentManager.updateAttributes(component);
		this.componentManager.updateModels(component);

		this.mapper.changeModel(this.currentComponent.getObjectId(), component.modelName);
		this.mapper.update();

		this.componentManager.updateStyles(component);
		component.draw();
	};

	updateCurrentComponent = () =>
	{
		this.updateProperties(this.currentComponent);
	};

	deleteComponent = () =>
	{
		let objectId = this.currentComponent.getObjectId();
		this.workbench.removeChild(this.currentComponent.element);
		this.mapper.removeComponent(objectId);

		this.componentManager.clearAttributes();
		this.componentManager.clearModelProperties();
		this.componentManager.clearStyles();
	};

	restore = (componentsStr:string) =>
	{
		let storage = JSON.parse(componentsStr);
		let $workbench = $(this.workbench);

		for (let objectId in storage.components)
		{
			let archivedComponent:ArchivedComponent = storage.components[objectId];
			let component = this.mapper.getComponent(objectId);
			let cell:Element = $workbench.find('.col[data-col-id="' + archivedComponent.colId + '"][data-row-id="' + archivedComponent.rowId + '"]')[0];
			this.createElement(component, cell);
			this.listProperties(component);

			component.element['model'] = this.mapper.getModelByModelName(component.modelName).model;
		}
	};

	createElement = (component:Component, dropTarget:Element) =>
	{
		component.create(this.onElementCreated, dropTarget);
		let element = component.element;
		element.addEventListener('click', function ()
		{
			this.currentComponent = this.mapper.getComponent((<any>element).dataset.id);
		}.bind(this));
	};
}
