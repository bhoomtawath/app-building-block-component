///<reference path="component-manager.ts"/>
///<reference path="component-services/attributes.ts"/>
///<reference path="component-services/model-properties.ts"/>
///<reference path="component-services/style-properties.ts"/>
///<reference path="../components/components.ts"/>
///<reference path="../component-factory.ts"/>
///<reference path="../../Mapper.ts"/>
/// <reference path="../../../../typings/jquery/jquery.d.ts" />
var Layout = (function () {
    function Layout(workbenchId, mapper) {
        var _this = this;
        this.registerDragTarget = function (draggedComponent) {
            _this.dragTarget = _this.mapper.componentFactory.getComponent(draggedComponent);
        };
        this.onDrag = function (ev) {
            var dragTarget = ev.target;
            var draggedComponent = dragTarget.dataset.component;
            _this.registerDragTarget(draggedComponent);
        };
        this.onDrop = function (ev, dropTarget) {
            ev.preventDefault();
            _this.createElement(_this.dragTarget, dropTarget);
        };
        this.onDragOver = function (ev) {
            ev.preventDefault();
            ev.dataTransfer.dropEffect = "move";
        };
        this.listProperties = function (component) {
            _this.componentManager.displayAttributes(component);
            _this.componentManager.displayModels(component);
            _this.componentManager.displayStyles(component);
        };
        this.onElementCreated = function (component) {
            _this.mapper.addComponent(component);
            var element = component.element;
            _this.onSelectObject(component);
            element.setAttribute('draggable', 'true');
            element.addEventListener('dragstart', _this.onDrag);
            element.addEventListener('click', function () {
                this.onSelectObject(component);
            }.bind(_this));
        };
        this.onSelectObject = function (component) {
            _this.currentComponent = component;
            _this.mapper.addComponent(component);
            var element = component.element;
            element.dataset.id = component.getObjectId();
            _this.listProperties(component);
        };
        this.updateProperties = function (component) {
            _this.componentManager.updateAttributes(component);
            _this.componentManager.updateModels(component);
            _this.mapper.changeModel(_this.currentComponent.getObjectId(), component.modelName);
            _this.mapper.update();
            _this.componentManager.updateStyles(component);
            component.draw();
        };
        this.updateCurrentComponent = function () {
            _this.updateProperties(_this.currentComponent);
        };
        this.deleteComponent = function () {
            var objectId = _this.currentComponent.getObjectId();
            _this.workbench.removeChild(_this.currentComponent.element);
            _this.mapper.removeComponent(objectId);
            _this.componentManager.clearAttributes();
            _this.componentManager.clearModelProperties();
            _this.componentManager.clearStyles();
        };
        this.restore = function (componentsStr) {
            var storage = JSON.parse(componentsStr);
            var $workbench = $(_this.workbench);
            for (var objectId in storage.components) {
                var archivedComponent = storage.components[objectId];
                var component = _this.mapper.getComponent(archivedComponent.componentId);
                var cell = $workbench.find('.col[data-col-id="' + archivedComponent.colId + '"][data-row-id="' + archivedComponent.rowId + '"]')[0];
                _this.createElement(component, cell);
                _this.listProperties(component);
            }
        };
        this.createElement = function (component, dropTarget) {
            component.create(_this.onElementCreated, dropTarget);
            var element = component.element;
            element.addEventListener('click', function () {
                this.currentComponent = this.mapper.getComponent(element.dataset.id);
            }.bind(_this));
        };
        this.workbench = document.getElementById(workbenchId);
        this.mapper = mapper;
        this.setUpComponentManager();
    }
    Layout.prototype.setUpComponentManager = function () {
        this.componentManager = new ComponentManager();
        this.componentManager.setAttributes(new Attributes(document.querySelector('#attribute-editor')));
        this.componentManager.setModelProperties(new ModelProperties(document.querySelector('#models-editor')));
        this.componentManager.setStyles(new StyleProperties(document.querySelector('#style-editor')));
    };
    return Layout;
}());
//# sourceMappingURL=layout.js.map