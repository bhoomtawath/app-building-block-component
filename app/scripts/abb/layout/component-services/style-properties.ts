///<reference path="properties.ts"/>
/// <reference path="../../../../../typings/jquery/jquery.d.ts" />

class StyleProperties implements Properties
{
	parentElement:JQuery;
	constructor(parentElement:Element)
	{
		this.parentElement = $(parentElement);
	}

	clearParentElement()
	{
		this.parentElement.empty();
	}

	display(component:Component)
	{
		this.parentElement.empty();
		$.each(component.style, (property, v) => {
			let $row = $('<div class="row property-row"></div>');
			let $label = $('<label class="form-label">' + v.property + '</label>');
			let $input = $('<input type="text" class="form-control property-value">');
			$input.val(v.value || '');
			$input.data('property', v.property);

			$row.append($label);
			$row.append($input);
			this.parentElement.append($row);
		});
	}

	update(component:Component)
	{
		let propertyRows = this.parentElement.find('.property-row');
		$.each(propertyRows, (k, v) => {
			let $row = $(v);
			let $inputBox = $row.find('.property-value').first();
			let key = $inputBox.data('property');
			let value = $inputBox.val();
			component.setStyleProperty(key, value);
		});
	}

}
