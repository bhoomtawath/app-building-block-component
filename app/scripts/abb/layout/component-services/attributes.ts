///<reference path="properties.ts"/>
/// <reference path="../../../../../typings/jquery/jquery.d.ts" />

class Attributes implements Properties
{
	parentElement:JQuery;
	constructor(parentElement:Element)
	{
		this.parentElement = $(parentElement);
	}

	clearParentElement()
	{
		this.parentElement.empty();
	}
	update(component:Component)
	{
		let propertyRows = this.parentElement.find('.property-row');
		$.each(propertyRows, (k, v) => {
			let $row = $(v);
			let $inputBox = $row.find('.property-value').first();
			let $checkbox = $row.find('.property-toggler').first();
			let enabled = $checkbox.is(':checked');
			let key = $inputBox.data('property');
			let value = $inputBox.val();
			component.setProperty(key, value, enabled);
		});
	}
	
	display(component:Component)
	{
		this.parentElement.empty();
		this.parentElement.show();

		$.each(component.properties, (k:string, v:ComponentProperty) => {
			let $row = $('<div class="row property-row"></div>');
			let $label = $('<label class="form-label">' + k + '</label>');
			let $input = $('<input type="text" class="form-control property-value">');
			$input.val(v.value || '');
			$input.data('property', k);
			let $enable = $('<input type="checkbox" class="property-toggler"/>');

			if (v.enabled) {
				$enable.prop('checked', true);
			}

			$row.append($enable);
			$row.append($label);
			$row.append($input);
			this.parentElement.append($row);
		});
	}
	
}
