///<reference path="../../components/components.ts"/>

interface Properties
{
	parentElement:JQuery;
	display(component:Component);
	update(component:Component);
	clearParentElement();
}
