///<reference path="properties.ts"/>
/// <reference path="../../../../../typings/jquery/jquery.d.ts" />

class ModelProperties implements Properties
{
	parentElement:JQuery;
	constructor(parentElement:Element)
	{
		this.parentElement = $(parentElement);
	}

	clearParentElement()
	{
		this.parentElement.empty();
	}

	display(component:Component)
	{
		this.parentElement.empty();
		this.parentElement.show();

		let modelName = component.modelName;
		let $row = $('<div class="row model-row"></div>');
		let $label = $('<label class="form-label">Model</label>');
		let $input = $('<input type="text" class="form-control model-value">');
		$input.val(modelName || '');

		$row.append($label);
		$row.append($input);
		this.parentElement.append($row);
	}

	update(component:Component)
	{
		let modelRows = this.parentElement.find('.model-row');
		component.modelName = modelRows.find('.model-value').val();
	}

}
