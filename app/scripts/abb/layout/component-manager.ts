///<reference path="component-services/properties.ts"/>
///<reference path="component-services/attributes.ts"/>
///<reference path="component-services/style-properties.ts"/>
///<reference path="component-services/model-properties.ts"/>



class ComponentManager
{
	private _attributes:Properties;
	private _styleProperties:Properties;
	private _modelProperties:Properties;

	getAttributes():Properties
	{
		return this._attributes;
	}

	setAttributes (attributes:Properties)
	{
		this._attributes = attributes;
	}

	getStyles():Properties
	{
		return this._styleProperties;
	}

	setStyles(styleProperties:Properties)
	{
		this._styleProperties = styleProperties;
	}

	getModelProperties():Properties
	{
		return this._modelProperties;
	}

	setModelProperties(value:Properties)
	{
		this._modelProperties = value;
	}

	displayAttributes(component:Component)
	{
		this._attributes.display(component);
	}

	updateAttributes(component:Component)
	{
		this._attributes.update(component);
	}

	clearAttributes()
	{
		this._attributes.clearParentElement();
	}

	displayStyles(component:Component)
	{
		this._styleProperties.display(component);
	}

	updateStyles(component:Component)
	{
		this._styleProperties.update(component);
	}

	clearStyles()
	{
		this._styleProperties.clearParentElement();
	}

	displayModels(component:Component)
	{
		this._modelProperties.display(component);
	}

	updateModels(component:Component)
	{
		this._modelProperties.update(component);
	}

	clearModelProperties()
	{
		this._modelProperties.clearParentElement();
	}
}
