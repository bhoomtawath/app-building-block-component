/// <reference path="component-property/component-property.ts"/>
/// <reference path="components.ts"/>
/// <reference path="basic-component.ts"/>


class DivComponent extends BasicComponent {

    static elementNo:number = 0;
    element:Element;
    componentId:string = 'div';
    properties:{
        [id: string] : ComponentProperty
    };
    style: {
        [property: string]: StyleProperty
    } = {};

    constructor(mapper:Mapper) {
        super(mapper);
    }

    newComponent(mapper:Mapper):Component {
        return new DivComponent(mapper);
    }

    create(callback:(component:Component) => void, parent?:Element) {
	    this.element = document.createElement('div');
        this.setObjectId(this.componentId + '_' + ++DivComponent.elementNo);
		this.setUpDefaultProperties();
        this.style['width'] = new StyleProperty('width', '100px');
        this.style['height'] =  new StyleProperty('height', '100px');
        this.style['border'] =  new StyleProperty('border', '1px solid');
		this.setPosition(parent);
        callback(this);
        if (parent) {
            this.insertElement(parent);
        }
        this.draw();
    }

    protected setUpDefaultProperties():void {
        super.setUpDefaultProperties();
    }
}
