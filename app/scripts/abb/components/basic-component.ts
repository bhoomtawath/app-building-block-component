///<reference path="components.ts"/>
///<reference path="component-property/component-property.ts"/>
///<reference path="../../../../typings/lodash/lodash.d.ts"/>
///<reference path="../style-property.ts"/>
///<reference path="../../ModelNode.ts"/>

class BasicComponent implements Component {
	static elementNo:number = 0;
    elementId:string;
    element:Element;
    componentId:string;
	objectId:string;
	modelName: string;
    properties:{
        [id: string] : ComponentProperty
    };
    style: {
        [property: string]: StyleProperty
    } = {};
	rowId:string;
	colId:string;
	mapper:Mapper;

    constructor(mapper:Mapper) {
        this.mapper = mapper;
        this.properties = {};
    }

    newComponent(mapper:Mapper):Component {
        return new BasicComponent(mapper);
    }

    onDrag() {
    }

    create(callback:(component:Component)=>void, parent?:Element) {
    }

    mapEntry() {
    }

    getComponentId():string {
        return this.componentId;
    }

    setProperty(key:string, value:any, enabled?:boolean):void {
        let property = this.properties[key];
        if (property && property.editable) {
            property.value = value;

            if (enabled != null) {
                property.enabled = enabled;
            }
        }
    }

    setStyleProperty(key:string, value:string):void {
        let property = this.style[key];
        if (property) {
            property.value = value;
        }
    }

    draw():void {
        _.forOwn(this.properties, function (v:ComponentProperty, k) {
            if (v.enabled && v.value != null) {
                this.element.setAttribute(k, v.value);
            } else {
                this.element.removeAttribute(k);
            }

        }.bind(this));

        this.updateStyle();
    }

    updateStyle() {
        _.forOwn(this.style, function(v:StyleProperty, k: string) {
			this.element.style[v.property] = v.value;
        }.bind(this));
    }

	updateModel(model:ModelNode) {
		this.modelName = model.name;
		this.element['model'] = model.model;
	}

	protected setPosition(parentElement:Element) {
		this.rowId = parentElement['dataset']['rowId'];
		this.colId = parentElement['dataset']['colId'];
	}

    protected setUpDefaultProperties() {
        this.properties['id'] = new ComponentProperty(this.elementId);
        this.properties['class'] = new ComponentProperty();
	    this.element['component'] = this;
	    this.element['droppable'] = false;
    }

    protected insertElement(parent?:Element) {
        parent.appendChild(this.element);
    }

    setObjectId(objectId:string):void {
	    this.objectId = objectId;
	    this.elementId = this.objectId;
    }

	getObjectId():string
	{
		return this.objectId;
	}

	updateModelValue(modelValue:any)
	{
		this.element['model'] = modelValue;
		this.mapper.updateModel();
	}

	restore(achieveComponent:ArchivedComponent)
	{
		this.objectId = achieveComponent.objectId;
		this.modelName = achieveComponent.modelName;
		this.colId = achieveComponent.colId;
		this.rowId = achieveComponent.rowId;
	}
}
