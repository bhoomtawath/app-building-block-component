/// <reference path="component-property/component-property.ts"/>
/// <reference path="components.ts"/>
/// <reference path="basic-component.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var DivComponent = (function (_super) {
    __extends(DivComponent, _super);
    function DivComponent(mapper) {
        _super.call(this, mapper);
        this.componentId = 'div';
        this.style = {};
    }
    DivComponent.prototype.newComponent = function (mapper) {
        return new DivComponent(mapper);
    };
    DivComponent.prototype.create = function (callback, parent) {
        this.element = document.createElement('div');
        this.setObjectId(this.componentId + '_' + ++DivComponent.elementNo);
        this.setUpDefaultProperties();
        this.style['width'] = new StyleProperty('width', '100px');
        this.style['height'] = new StyleProperty('height', '100px');
        this.style['border'] = new StyleProperty('border', '1px solid');
        this.setPosition(parent);
        callback(this);
        if (parent) {
            this.insertElement(parent);
        }
        this.draw();
    };
    DivComponent.prototype.setUpDefaultProperties = function () {
        _super.prototype.setUpDefaultProperties.call(this);
    };
    DivComponent.elementNo = 0;
    return DivComponent;
}(BasicComponent));
//# sourceMappingURL=div-component.js.map