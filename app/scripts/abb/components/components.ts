///<reference path="component-property/component-property.ts"/>
///<reference path="../style-property.ts"/>
///<reference path="../../ModelNode.ts"/>
///<reference path="../layout/layout.ts"/>


interface Component {

    element:Element;
	modelName: string;
    elementId: string;
    properties: {
        [id: string] : ComponentProperty
    };
    style: {
        [property: string]: StyleProperty
    };
	rowId: string;
	colId: string
	mapper:Mapper;

    newComponent(mapper:Mapper):Component;

    onDrag():void;

    create(callback:(component:Component) => void, parent?:Element):void;

    mapEntry():void;
	
	getComponentId(): string;
	
    setProperty(key:string, value:any, enabled?:boolean);

	setStyleProperty(key:string, value:string): void;

    draw(callback?):void;

	setObjectId(objectId:string): void;

	getObjectId():string;

	updateModelValue(model:any):void

	restore(achieveComponent: ArchivedComponent);
}
