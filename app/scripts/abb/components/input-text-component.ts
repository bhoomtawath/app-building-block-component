/// <reference path="component-property/component-property.ts"/>
/// <reference path="components.ts"/>
/// <reference path="basic-component.ts"/>


class InputTextComponent extends BasicComponent {

    static elementNo:number = 0;
    element;
    componentId:string = 'input-text';
    properties:{
        [id: string] : ComponentProperty
    };

    constructor(mapper:Mapper) {
        super(mapper);
    }

    newComponent(mapper:Mapper):Component {
        return new InputTextComponent(mapper);
    }

    create(callback:(component:Component) => void, parent?:Element) {
	    this.element = document.createElement('input-text');
        this.setObjectId(this.componentId + '_' + ++InputTextComponent.elementNo);
		this.setUpDefaultProperties();
	    this.setPosition(parent);
        //TODO: change this to the workbench
        callback(this);
        this.insertElement(parent);
        this.draw();
    }

    protected setUpDefaultProperties():void {
        super.setUpDefaultProperties();
    }
}
