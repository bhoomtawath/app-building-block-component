/// <reference path="component-property/component-property.ts"/>
/// <reference path="components.ts"/>
/// <reference path="basic-component.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var InputTextComponent = (function (_super) {
    __extends(InputTextComponent, _super);
    function InputTextComponent(mapper) {
        _super.call(this, mapper);
        this.componentId = 'input-text';
    }
    InputTextComponent.prototype.newComponent = function (mapper) {
        return new InputTextComponent(mapper);
    };
    InputTextComponent.prototype.create = function (callback, parent) {
        this.element = document.createElement('input-text');
        this.setObjectId(this.componentId + '_' + ++InputTextComponent.elementNo);
        this.setUpDefaultProperties();
        this.setPosition(parent);
        //TODO: change this to the workbench
        callback(this);
        this.insertElement(parent);
        this.draw();
    };
    InputTextComponent.prototype.setUpDefaultProperties = function () {
        _super.prototype.setUpDefaultProperties.call(this);
    };
    InputTextComponent.elementNo = 0;
    return InputTextComponent;
}(BasicComponent));
//# sourceMappingURL=input-text-component.js.map