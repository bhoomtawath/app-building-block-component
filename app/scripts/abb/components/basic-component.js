///<reference path="components.ts"/>
///<reference path="component-property/component-property.ts"/>
///<reference path="../../../../typings/lodash/lodash.d.ts"/>
///<reference path="../style-property.ts"/>
///<reference path="../../ModelNode.ts"/>
var BasicComponent = (function () {
    function BasicComponent(mapper) {
        this.style = {};
        this.mapper = mapper;
        this.properties = {};
    }
    BasicComponent.prototype.newComponent = function (mapper) {
        return new BasicComponent(mapper);
    };
    BasicComponent.prototype.onDrag = function () {
    };
    BasicComponent.prototype.create = function (callback, parent) {
    };
    BasicComponent.prototype.mapEntry = function () {
    };
    BasicComponent.prototype.getComponentId = function () {
        return this.componentId;
    };
    BasicComponent.prototype.setProperty = function (key, value, enabled) {
        var property = this.properties[key];
        if (property && property.editable) {
            property.value = value;
            if (enabled != null) {
                property.enabled = enabled;
            }
        }
    };
    BasicComponent.prototype.setStyleProperty = function (key, value) {
        var property = this.style[key];
        if (property) {
            property.value = value;
        }
    };
    BasicComponent.prototype.draw = function () {
        _.forOwn(this.properties, function (v, k) {
            if (v.enabled && v.value != null) {
                this.element.setAttribute(k, v.value);
            }
            else {
                this.element.removeAttribute(k);
            }
        }.bind(this));
        this.updateStyle();
    };
    BasicComponent.prototype.updateStyle = function () {
        _.forOwn(this.style, function (v, k) {
            this.element.style[v.property] = v.value;
        }.bind(this));
    };
    BasicComponent.prototype.updateModel = function (model) {
        this.modelName = model.name;
        this.element['model'] = model.model;
    };
    BasicComponent.prototype.setPosition = function (parentElement) {
        this.rowId = parentElement['dataset']['rowId'];
        this.colId = parentElement['dataset']['colId'];
    };
    BasicComponent.prototype.setUpDefaultProperties = function () {
        this.properties['id'] = new ComponentProperty(this.elementId);
        this.properties['class'] = new ComponentProperty();
        this.element['component'] = this;
        this.element['droppable'] = false;
    };
    BasicComponent.prototype.insertElement = function (parent) {
        parent.appendChild(this.element);
    };
    BasicComponent.prototype.setObjectId = function (objectId) {
        this.objectId = objectId;
        this.elementId = this.objectId;
    };
    BasicComponent.prototype.getObjectId = function () {
        return this.objectId;
    };
    BasicComponent.prototype.updateModelValue = function (modelValue) {
        this.element['model'] = modelValue;
        this.mapper.updateModel();
    };
    BasicComponent.prototype.restore = function (achieveComponent) {
        this.objectId = achieveComponent.objectId;
        this.modelName = achieveComponent.modelName;
    };
    BasicComponent.elementNo = 0;
    return BasicComponent;
}());
//# sourceMappingURL=basic-component.js.map