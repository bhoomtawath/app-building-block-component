/// <reference path="component-property/component-property.ts"/>
/// <reference path="components.ts"/>
/// <reference path="basic-component.ts"/>

class ABBTableComponent extends BasicComponent {
	static elementNo:number = 0;
	elementId:string;
	element:Element;
	componentId:string = 'abb-table';
	componentClass:string;
	properties:{[id:string]: ComponentProperty};
	style:{[property: string]: StyleProperty}  = {};

	constructor(mapper:Mapper) {
		super(mapper);
	}

	create(callback:(component:Component)=>void, parent?:Element):void {
		this.element = document.createElement('abb-table');
		this.setObjectId(this.componentId + '_' + ++ABBTableComponent.elementNo);
		this.setUpDefaultProperties();
		this.style['width'] = new StyleProperty('width', '100px');
		this.style['height'] =  new StyleProperty('height', '100px');
		this.style['border'] =  new StyleProperty('border', '1px solid');

		callback(this);
		if (parent) {
			this.insertElement(parent);
		}
		this.draw();
	}

	newComponent(mapper:Mapper):Component {
		return new ABBTableComponent(mapper);
	}

	protected setUpDefaultProperties() {
		super.setUpDefaultProperties();
		this.properties['headers'] = new ComponentProperty();
		this.properties['footer'] = new ComponentProperty();
	}

	updateStyle() {
		let tableWrapperStyle:CSSStyleDeclaration = (<HTMLElement>this.element.querySelector('div')).style;
		tableWrapperStyle.width = this.style['width'].value;
		tableWrapperStyle.height = this.style['height'].value;
		tableWrapperStyle.border = this.style['border'].value;
		tableWrapperStyle.display = 'inline-block';
	}

	draw():void {
		super.draw();
	}
}
