///<reference path="components.ts"/>
///<reference path="basic-component.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var PaperIconButtonComponent = (function (_super) {
    __extends(PaperIconButtonComponent, _super);
    function PaperIconButtonComponent(mapper) {
        _super.call(this, mapper);
        this.componentId = 'paper-icon-button';
    }
    PaperIconButtonComponent.prototype.newComponent = function (mapper) {
        _super.prototype.newComponent.call(this, mapper);
        return new PaperIconButtonComponent(mapper);
    };
    PaperIconButtonComponent.prototype.create = function (callback, parent) {
        this.element = document.createElement('paper-icon-button');
        this.setObjectId(this.componentId + '_' + PaperIconButtonComponent.elementNo++);
        this.setUpDefaultProperties();
        callback(this);
        this.insertElement(parent);
        this.draw();
    };
    PaperIconButtonComponent.prototype.setUpDefaultProperties = function () {
        _super.prototype.setUpDefaultProperties.call(this);
        this.properties['icon'] = new ComponentProperty('polymer');
        this.properties['src'] = new ComponentProperty();
    };
    PaperIconButtonComponent.elementNo = 0;
    return PaperIconButtonComponent;
}(BasicComponent));
//# sourceMappingURL=paper-icon-button-component.js.map