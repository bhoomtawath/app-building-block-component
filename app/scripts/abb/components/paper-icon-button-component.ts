///<reference path="components.ts"/>
///<reference path="basic-component.ts"/>


class PaperIconButtonComponent extends BasicComponent implements Component {

    static elementNo:number = 0;
    componentId:string = 'paper-icon-button';

    constructor(mapper:Mapper) {
        super(mapper);
    }

    newComponent(mapper:Mapper):BasicComponent {
        super.newComponent(mapper);
        return new PaperIconButtonComponent(mapper);
    }

    create(callback:(component:Component)=>void, parent?:Element) {
	    this.element = document.createElement('paper-icon-button');
        this.setObjectId(this.componentId + '_' + PaperIconButtonComponent.elementNo++);
		this.setUpDefaultProperties();

        callback(this);
        this.insertElement(parent);
        this.draw();
    }

    protected setUpDefaultProperties() {
        super.setUpDefaultProperties();
        this.properties['icon'] = new ComponentProperty('polymer');
        this.properties['src'] = new ComponentProperty();
    }
}
