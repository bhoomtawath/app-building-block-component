class ComponentProperty {
    value: string;
    editable: boolean;
    type: string;
    enabled: boolean;

    constructor(value?: string, editable?: boolean, type?: string, enabled?: boolean) {
        this.value = value;
        this.editable = editable || true;
        this.type = type || 'string';
        this.enabled = enabled || true;
    }
};
