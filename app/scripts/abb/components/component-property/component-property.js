var ComponentProperty = (function () {
    function ComponentProperty(value, editable, type, enabled) {
        this.value = value;
        this.editable = editable || true;
        this.type = type || 'string';
        this.enabled = enabled || true;
    }
    return ComponentProperty;
}());
;
//# sourceMappingURL=component-property.js.map