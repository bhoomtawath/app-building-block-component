///<reference path="components/components.ts"/>
///<reference path="components/input-text-component.ts"/>
///<reference path="components/paper-icon-button-component.ts"/>
///<reference path="components/div-component.ts"/>
///<reference path="components/abb-table-component.ts"/>
///<reference path="layout.ts"/>
var ComponentFactory = (function () {
    function ComponentFactory(mapper) {
        this.mapper = mapper;
        this.components = {};
        this.setUpComponents();
    }
    ComponentFactory.prototype.registerComponent = function (component) {
        this.components[component.getComponentId()] = component;
    };
    ComponentFactory.prototype.getComponent = function (componentId) {
        var component = this.components[componentId];
        return component.newComponent(this.mapper);
    };
    ComponentFactory.prototype.setUpComponents = function () {
        this.registerComponent(new InputTextComponent(this.mapper));
        this.registerComponent(new PaperIconButtonComponent(this.mapper));
        this.registerComponent(new DivComponent(this.mapper));
        this.registerComponent(new ABBTableComponent(this.mapper));
    };
    return ComponentFactory;
}());
//# sourceMappingURL=component-factory.js.map