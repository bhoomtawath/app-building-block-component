///<reference path="components/components.ts"/>
///<reference path="components/input-text-component.ts"/>
///<reference path="components/paper-icon-button-component.ts"/>
///<reference path="components/div-component.ts"/>
///<reference path="components/abb-table-component.ts"/>
///<reference path="layout/layout.ts"/>

class ComponentFactory {
	components:{
		[id:string]:Component;
	};
	mapper:Mapper;

	constructor(mapper:Mapper) {
		this.mapper = mapper;
		this.components = {};
		this.setUpComponents();
	}

	registerComponent(component:Component) {
		this.components[component.getComponentId()] = component;
	}

	getComponent(componentId:string):Component {
		var component:Component = this.components[componentId];
		return component.newComponent(this.mapper);
	}

	private setUpComponents():void {
		this.registerComponent(new InputTextComponent(this.mapper));
		this.registerComponent(new PaperIconButtonComponent(this.mapper));
		this.registerComponent(new DivComponent(this.mapper));
		this.registerComponent(new ABBTableComponent(this.mapper));
	}
}
