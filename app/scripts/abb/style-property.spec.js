describe('StyleProperty', function() {
  it('should have property and value in constructor', function() {
    var styleProperty = new StyleProperty('property', 'value');
    expect(styleProperty.property).toBe('property');
  });
});
