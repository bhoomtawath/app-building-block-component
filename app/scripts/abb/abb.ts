/// <reference path="../../../typings/jquery/jquery.d.ts" />
///<reference path="layout/layout.ts"/>
///<reference path="../Mapper.ts"/>

let universe = {};
let mapper = new Mapper(universe);
(<any>window).layout = new Layout('workbench', mapper);

$(document).ready(function () {
	let $workbench = $('#workbench');
	$workbench.on('click', '*', function (ev) {
		ev.preventDefault();
	}).on('focus', '*', function (ev) {
		ev.preventDefault();
	});
});
