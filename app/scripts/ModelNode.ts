class ModelNode {
	type:string;
	model:any;
	status:any;
	inputs:any[];
	outputs:any[];
	modelName:string;
	name:string;

	constructor(name:string, model:any) {
		this.name = name;
		this.model = model;
	}

	getModel():any {
		return this.model;
	}
	setModel(model:any):void {
		this.model = model;
	}
}
