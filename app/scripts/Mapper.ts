///<reference path="abb/components/components.ts"/>
interface MapperComponent
{
	component:Component,
	modelName:string
}

interface ArchivedComponent
{
	elementId:string;
	componentId:string;
	objectId:string;
	modelName:string;
	properties:{
		[id:string]:string
	};
	rowId:string;
	colId:string;
}

class Mapper
{
	universe:any;
	componentFactory:ComponentFactory;
	components:{[componentId:string]:MapperComponent};

	constructor(universe)
	{
		this.universe = universe;
		this.components = {};
		this.componentFactory = new ComponentFactory(this);
	}

	update()
	{
		// for (let componentId in this.components) {
		// 	var component =  this.components[componentId];
		// 	var _modelName = component.modelName;
		// 	var model = this.universe.find(function (m) {
		// 		return m.name === _modelName;
		// 	});
		// 	if (model) {
		// 		component.component.updateModel(model);
		// 	}
		// }
	}

	getModelByModelName(modelName:string)
	{

		let model = this.universe.find(function (m) {
			return m.name === modelName;
		});

		return model;
	}

	addComponent(component:Component)
	{
		this.components[component.getObjectId()] = {
			component: component,
			modelName: component.modelName
		};

		this.updateModel();
	}

	updateModel()
	{
		// for (let componentId in this.components) {
		// 	var component =  this.components[componentId];
		// 	var _modelName = component.modelName;
		// 	var model = this.universe.find(function (m) {
		// 		return m.name === _modelName;
		// 	});
		// 	if (model) {
		// 		model.model = component.component.element['model'];
		// 	}
		// }
	}

	getComponent(objectId:string)
	{
		return this.components[objectId].component;
	}

	changeModel(objectId:string, modelName:string)
	{
		this.components[objectId].modelName = modelName;
	}

	removeComponent(objectId:string)
	{
		delete this.components[objectId];
	}

	serialize()
	{
		let components = {};

		for (let componentID in this.components)
		{
			let component:Component = this.components[componentID].component;
			let properties: {
				[id:string]: string
			} = {};

			for (let properyKey in component.properties)
			{
				let property = component.properties[properyKey];
				properties[properyKey] = property.value;
			}

			let archiveComponent:ArchivedComponent = {
				rowId: component.rowId,
				colId: component.colId,
				objectId: component.getObjectId(),
				elementId: component.elementId,
				componentId: component.getComponentId(),
				modelName: component.modelName,
				properties: properties
			};

			components[componentID] = archiveComponent;
		}

		let archive = {
			components: components
		};

		return JSON.stringify(archive);
	}

	deserialize(serializedMapper:string)
	{
		this.universe = window['universe'];
		let deserialized = JSON.parse(serializedMapper);

		for (let objectId in deserialized.components)
		{
			let archivedComponent:ArchivedComponent = deserialized.components[objectId];
			let component = this.componentFactory.getComponent(archivedComponent.componentId);
			component.restore(archivedComponent);
			this.addComponent(component);
		}
	}
}


